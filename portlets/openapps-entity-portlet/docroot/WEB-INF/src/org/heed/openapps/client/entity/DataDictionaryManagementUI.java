package org.heed.openapps.client.entity;
import java.util.List;

import org.heed.openapps.dictionary.service.HttpDataDictionaryService;
import org.heed.openapps.dictionary.Model;

import com.liferay.portal.kernel.util.PropsUtil;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;


public class DataDictionaryManagementUI extends UI {
	private static final long serialVersionUID = -3663310029434675362L;
	public static final String OPENAPPS_URL = PropsUtil.get("openapps.url");
	private HttpDataDictionaryService dictionaryService;
	
	
	@Override
	protected void init(VaadinRequest request) {
		dictionaryService = new HttpDataDictionaryService(OPENAPPS_URL, "opensearch");
		
		Panel panel = new Panel();
		panel.setStyleName(Reindeer.PANEL_LIGHT);
		panel.setSizeUndefined();
		setContent(panel);
				
		VerticalLayout layout = new VerticalLayout();
    	panel.setContent(layout);
        
        try {        	        	
        	List<Model> models = dictionaryService.getModels();
        	
        	for(Model model : models) {
        		Label label = new Label(model.getQName().toString());
        		label.setHeight("35px");
        		layout.addComponent(label);
        	}
        	
        } catch(Exception e) {
        	e.printStackTrace();
        }
        
        layout.setSizeUndefined();
	}

}
