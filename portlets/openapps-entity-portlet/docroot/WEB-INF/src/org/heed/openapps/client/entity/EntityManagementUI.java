package org.heed.openapps.client.entity;

import org.heed.openapps.SystemModel;
import org.heed.openapps.entity.BaseEntityQuery;
import org.heed.openapps.entity.Entity;
import org.heed.openapps.entity.EntityQuery;
import org.heed.openapps.entity.EntityResultSet;
import org.heed.openapps.entity.service.HttpEntityService;

import com.liferay.portal.kernel.util.PropsUtil;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.Reindeer;


public class EntityManagementUI extends UI {
	private static final long serialVersionUID = 2868741818925663729L;
	private HttpEntityService entityService;
	public static final String OPENAPPS_URL = PropsUtil.get("openapps.url");
	
	
	@Override
	protected void init(VaadinRequest request) {
		entityService = new HttpEntityService(OPENAPPS_URL, "opensearch");
		Panel panel = new Panel();
		panel.setStyleName(Reindeer.PANEL_LIGHT);
		panel.setSizeUndefined();
		setContent(panel);
		 
		
		// Create the content
		FormLayout content = new FormLayout();
		//content.addStyleName("mypanelcontent");
		content.addComponent(new TextField("Participant"));
		content.addComponent(new TextField("Organization"));
		content.setSizeUndefined(); // Shrink to fit
		content.setMargin(true);
		panel.setContent(content);
		
		VerticalLayout layout = new VerticalLayout();
    	panel.setContent(layout);
        
        try {        	        	
        	EntityQuery query = new BaseEntityQuery(SystemModel.USER);
        	EntityResultSet results = entityService.search(query);
        	
        	for(Entity entity : results.getResults()) {
        		Label label = new Label(entity.getQName().toString());
        		label.setHeight("35px");
        		layout.addComponent(label);
        		
        	}
        	
        } catch(Exception e) {
        	e.printStackTrace();
        }
        
        layout.setSizeUndefined();
    }
}
