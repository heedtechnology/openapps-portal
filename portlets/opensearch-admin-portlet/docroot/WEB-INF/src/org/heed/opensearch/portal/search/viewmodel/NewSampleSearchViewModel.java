package org.heed.opensearch.portal.search.viewmodel;

import com.vaadin.data.util.AbstractProperty;

@SuppressWarnings("serial")
public class NewSampleSearchViewModel extends AbstractProperty<String> {

	private SampleSearchesViewModel sampleSearchesViewModel;

	NewSampleSearchViewModel(SampleSearchesViewModel sampleSearchesViewModel) {
		super();
		this.sampleSearchesViewModel = sampleSearchesViewModel;
	}

	@Override
	public String getValue() {
		return this.sampleSearchesViewModel.getNewSearch();
	}

	@Override
	public void setValue(String newValue){
		this.sampleSearchesViewModel.setNewSearch(newValue);
	}

	@Override
	public Class<? extends String> getType() {
		return String.class;
	}
}
