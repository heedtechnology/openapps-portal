package org.heed.opensearch.search;

/**
 * Created by babar on 1/19/14.
 */
public interface ValueGetter<T> {

    public void getValue(T value);
}
