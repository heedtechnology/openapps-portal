package org.heed.opensearch.portal.search.viewmodel;

import org.heed.opensearch.search.PropertyDefinition;
import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxOWLObjectRendererImpl;

public class PropertyDefinitionViewModel extends DefinitionViewModel<PropertyDefinition> {
	PropertyDefinitionViewModel(PropertyDefinition propertyDefinition) {
		super(propertyDefinition);
	}
}
