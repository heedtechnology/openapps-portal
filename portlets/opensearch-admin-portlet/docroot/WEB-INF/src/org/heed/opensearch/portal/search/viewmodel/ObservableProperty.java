package org.heed.opensearch.portal.search.viewmodel;

import java.util.ArrayList;
import java.util.List;

public abstract class ObservableProperty<ValueType> {

	private final List<ValueSetListener<ValueType>> valueSetListenerList = new ArrayList<ValueSetListener<ValueType>>();
	private boolean isDuringSet;
	private boolean isValueSetDuringSet;
	private ValueType value;
	private ValueType valueDuringSet;
	
	public ObservableProperty(ValueType value) {
		this.value = value;
	}
	
	public void addValueSetListener(ValueSetListener<ValueType> valueSetListener){
		valueSetListenerList.add(valueSetListener);
	}
	
	public final void setValue(ValueType newValue) {
		
		if(isDuringSet) {
			isValueSetDuringSet = true;
			valueDuringSet = newValue;
			return;
		}
		
		if(!OnBeforeSet(value, newValue))
			return;
		
		isDuringSet = true;
		ValueType currentValue = value;
		value = newValue;
		
		for (ValueSetListener<ValueType> listener : valueSetListenerList)
			listener.valueSet(this, currentValue, newValue);
		
		isDuringSet = false;
		OnAfterSet();
		
		if(isValueSetDuringSet){
			isValueSetDuringSet = false;
			setValue(valueDuringSet);
		}
	}
	
	protected abstract boolean OnBeforeSet(ValueType currentValue, ValueType newValue);
	
	protected abstract void OnAfterSet();

	public ValueType getValue(){
		return value;
	}
}
