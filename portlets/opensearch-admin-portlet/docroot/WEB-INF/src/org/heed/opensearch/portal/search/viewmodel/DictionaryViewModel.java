package org.heed.opensearch.portal.search.viewmodel;

import org.heed.opensearch.search.PizzaDictionaryProvider;
import org.heed.opensearch.search.Definition;
import org.heed.opensearch.search.DefinitionIdBase;
import org.heed.opensearch.search.Dictionary;
import org.heed.opensearch.search.ClassDefintion;
import org.heed.opensearch.search.PropertyDefinition;
import org.heed.opensearch.search.TryResult;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.vaadin.data.util.BeanItemContainer;

@SuppressWarnings("serial")
public final class DictionaryViewModel extends
		BeanItemContainer<DefinitionIdViewModel> implements
		ValueSetListener<Object> {

	private final ViewModelProperty<Object> selectedDefinitionId;
	private final Dictionary<ClassDefintion> classDictionary;
	private final Dictionary<PropertyDefinition<?, ?>> propertyDictionary;

	public DictionaryViewModel() {

		super(DefinitionIdViewModel.class);

		Dictionary<ClassDefintion> dictionary;
		Dictionary<PropertyDefinition<?, ?>> propertyDictionary;
		ViewModelProperty<Object> selectedDefinitionId;
		
		try {
			dictionary = PizzaDictionaryProvider.provideDictionaryOfClassDefinition();
			
			for (Definition<? extends DefinitionIdBase> definition : dictionary.getDefinitions())
				addBean(new DefinitionIdViewModel(definition.getId()));
			
			propertyDictionary = PizzaDictionaryProvider
					.provideDictionaryOfPropertyDefinition();
			
			for (Definition<? extends DefinitionIdBase> definition : propertyDictionary.getDefinitions())
				addBean(new DefinitionIdViewModel(definition.getId()));
			
			selectedDefinitionId = new ViewModelProperty<Object>(size() == 0
					? null : getIdByIndex(0));
			selectedDefinitionId.addValueSetListener(this);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();

			dictionary = new Dictionary<ClassDefintion>();
			propertyDictionary = new Dictionary<PropertyDefinition<?, ?>>();
			selectedDefinitionId = null;
		}
		
		classDictionary = dictionary;
		this.propertyDictionary = propertyDictionary;
		this.selectedDefinitionId = selectedDefinitionId;
	}

	public ViewModelProperty<Object> getSelectedDefinitionId() {
		return selectedDefinitionId;
	}

	@Override
	public void valueSet(ObservableProperty<Object> property,
			Object currentValue, Object newValue) {
		if (newValue == null && currentValue != null)
			property.setValue(currentValue);
	}
	
	public DefinitionViewModel getDefintion(DefinitionIdViewModel definitionIdViewModel) {
		if(propertyDictionary.containsDefintionWithId(definitionIdViewModel.getDefinitionId()))
			return new PropertyDefinitionViewModel(propertyDictionary.get(definitionIdViewModel.getDefinitionId()));

		return new ClassDefintionViewModel(classDictionary.get(definitionIdViewModel.getDefinitionId()));

	}
}
