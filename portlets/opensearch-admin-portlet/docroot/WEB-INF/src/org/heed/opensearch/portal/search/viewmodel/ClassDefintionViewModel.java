package org.heed.opensearch.portal.search.viewmodel;

import org.heed.opensearch.search.ClassDefintion;

import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxOWLObjectRendererImpl;

public class ClassDefintionViewModel extends DefinitionViewModel<ClassDefintion> {
	public ClassDefintionViewModel(ClassDefintion definition) {
		super(definition);
	}
}
