package org.heed.opensearch.search;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.TreeMap;

public final class Dictionary<DefinitionType extends Definition<? extends DefinitionIdBase>> {
	
	private final TreeMap<Integer, DefinitionType> definitions = new TreeMap<Integer, DefinitionType>();
	private final TreeMap<String, List<DefinitionIdBase>> nameIdListMap = new TreeMap<String, List<DefinitionIdBase>>();
	private final TreeMap<String, List<DefinitionIdBase>> labelIdListMap = new TreeMap<String, List<DefinitionIdBase>>();
	
	public List<DefinitionType> lookupByName(String name) {
		return idListToDefinitionList(nameIdListMap.get(name));
	}

	public List<DefinitionType> lookupByLabel(String label) {
		return idListToDefinitionList(labelIdListMap.get(label));
	}
	
	private List<DefinitionType> idListToDefinitionList(List<DefinitionIdBase> idList) {
		
		ArrayList<DefinitionType> definitions = new ArrayList<DefinitionType>();
		
		for (DefinitionIdBase definitionIdBase : idList)
			definitions.add(this.definitions.get(definitionIdBase));	
		
		return definitions;
	}

	public void addDefinition(DefinitionType definition) {
		definitions.put(definition.getId().getHashCode(), definition);
	}

	public Collection<DefinitionType> getDefinitions() {
		return definitions.values();
	}

	public boolean containsDefintionWithId(DefinitionIdBase definitionId) {
		return definitions.containsKey(definitionId.getHashCode());
	}

	public DefinitionType get(DefinitionIdBase definitionId) {
		return definitions.get(definitionId.getHashCode());
	}
}
