/*
 * Copyright (C) 2010 Heed Technology Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */
package org.heed.opensearch.search;

public abstract class DefinitionBase<DefinitionIdType extends DefinitionIdBase, DefinitionExpressionType extends DefinitionExpression>
	extends Definition<DefinitionIdType> {
	
	private final DefinitionIdType definitionId;
	private final DefinitionExpressionType expression;
	private final boolean caseSensitive;
	
	DefinitionBase(DefinitionIdType id, DefinitionExpressionType expression) {
		super(id);
		this.definitionId = id;
		this.expression = expression;
		this.caseSensitive = true;
	}

	public DefinitionIdType getId() {
		return definitionId;
	}

	DefinitionExpressionType getExpression() {
		return expression;
	}
}
