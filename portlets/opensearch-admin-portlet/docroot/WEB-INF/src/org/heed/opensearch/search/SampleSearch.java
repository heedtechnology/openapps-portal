package org.heed.opensearch.search;

import java.util.Iterator;

import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.iterators.FilterIterator;

public final class SampleSearch {
	private final Iterable<SampleSearchToken> phrases;
	
	SampleSearch(Iterable<SampleSearchToken> phrases) {
		super();
		this.phrases = phrases;
	}

	public Iterable<SampleSearchToken> getPhrases() {
		return phrases;
	}
	
	@SuppressWarnings("unchecked")
	public Iterable<SampleSearchToken> getUnknownPhrases() {
		Predicate predicate = new Predicate() {
            public boolean evaluate(Object obj) { 
            	return ((SampleSearchToken)obj).getType() == SampleSearchToken.TYPE_UNKNOWN;
            }
		};
		Iterable<SampleSearchToken> iterator = (Iterable<SampleSearchToken>)new FilterIterator((Iterator<SampleSearchToken>)phrases, predicate);
		return iterator;
	}

	@SuppressWarnings("unchecked")
	public Iterable<SampleSearchToken> getKnownPhrases() {
		Predicate predicate = new Predicate() {
            public boolean evaluate(Object obj) { 
            	return ((SampleSearchToken)obj).getType() == SampleSearchToken.TYPE_KNOWN;
            }
		};
		Iterable<SampleSearchToken> iterator = (Iterable<SampleSearchToken>)new FilterIterator((Iterator<SampleSearchToken>)phrases, predicate);
		return iterator;
	}
}
