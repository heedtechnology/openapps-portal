package org.heed.opensearch.search;

public abstract class DefinitionIdBase implements DefintionId {
	
	DefinitionIdBase() {
	}

	public abstract String getName();
	public abstract boolean hasLabel();
	public abstract String getLabel();
	public abstract String toString();
	public abstract int getHashCode();
	public abstract boolean equals(DefinitionIdBase definitionId);	
}
