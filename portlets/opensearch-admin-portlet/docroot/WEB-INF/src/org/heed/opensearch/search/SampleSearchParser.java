package org.heed.opensearch.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

import org.codehaus.jparsec.Parser;
import org.codehaus.jparsec.Parsers;
import org.codehaus.jparsec.Scanners;
import org.codehaus.jparsec.Token;
import org.codehaus.jparsec.Tokens;
import org.codehaus.jparsec.Tokens.Fragment;
import org.codehaus.jparsec.functors.Map;
import org.codehaus.jparsec.pattern.CharPredicate;
import org.codehaus.jparsec.pattern.CharPredicates;
import org.codehaus.jparsec.pattern.Patterns;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public final class SampleSearchParser {
	private static final String DEFINTION = "defintion";
	private static final String UNKNOWN = "unknown";
	private static final String DATE = "date";
	private final PizzaDictionaryProvider pizzaDictionaryProvider = new PizzaDictionaryProvider();
	private final Parser<List<Token>> tokenizer;

	public SampleSearchParser() throws OWLOntologyCreationException {
		HashSet<String> defintionNames = new HashSet<String>();

		final Collection<ClassDefintion> classDefinitions = pizzaDictionaryProvider
				.provideDictionaryOfClassDefinition().getDefinitions();
		final Collection<PropertyDefinition<?, ?>> propertyDefinitions = pizzaDictionaryProvider
				.provideDictionaryOfPropertyDefinition().getDefinitions();

		for (PropertyDefinition<?, ?> propertyDefinition : propertyDefinitions)
			defintionNames.add(propertyDefinition.getId().getName());

		for (ClassDefintion classDefintion : classDefinitions)
			defintionNames.add(classDefintion.getId().getName());

		final Parser<Fragment> definitionTokenizer = new DefinitionParserFactory(
				defintionNames).CreateParserInstance().source()
				.map(new Map<String, Fragment>() {

					@Override
					public Fragment map(String from) {
						return Tokens.fragment(from, DEFINTION);
					}
				});

		final Parser<Fragment> dateScanner = DateScannersFactory.createDateScanner()
				.source().map(new Map<String, Fragment>() {
		
					@Override
					public Fragment map(String from) {
						return Tokens.fragment(from, DATE);
					}
					
				});
		
		final Parser<Fragment> unknownTokenizer = Scanners
				.many1(new CharPredicate() {

					@Override
					public boolean isChar(char c) {
						return !CharPredicates.IS_WHITESPACE.isChar(c);
					}
				}).source().map(new Map<String, Fragment>() {

					@Override
					public Fragment map(String from) {
						return Tokens.fragment(from, UNKNOWN);
					}
				});

		tokenizer = Parsers.or(definitionTokenizer, dateScanner, unknownTokenizer).lexer(
				Scanners.pattern(Patterns.many(CharPredicates.IS_WHITESPACE),
						"whitespaces"));
	}

	public SampleSearch parse(String value) {
		final List<Token> tokenList = tokenizer.parse(value);

		List<SampleSearchToken> definitions = new ArrayList<SampleSearchToken>();

		final Token[] tokenArray = tokenList.toArray(new Token[0]);

		Arrays.sort(tokenArray, new Comparator<Token>() {

			@Override
			public int compare(Token o1, Token o2) {
				return Integer.compare(o1.index(), o2.index());
			}

		});

		for (int i = 0; i < tokenArray.length; i++) {
			Fragment fragment = (Fragment) tokenArray[i].value();
			final SampleSearchToken sampleSearchToken = new SampleSearchToken(tokenArray, i);
			if (fragment.tag() == DEFINTION)
				sampleSearchToken.setType(SampleSearchToken.TYPE_KNOWN);
			else if (fragment.tag() == UNKNOWN)
				sampleSearchToken.setType(SampleSearchToken.TYPE_UNKNOWN);
			definitions.add(sampleSearchToken);
		}

		return new SampleSearch(definitions);
	}
}
