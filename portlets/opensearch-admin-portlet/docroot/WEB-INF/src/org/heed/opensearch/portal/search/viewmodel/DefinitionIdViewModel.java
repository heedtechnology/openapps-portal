package org.heed.opensearch.portal.search.viewmodel;

import org.heed.opensearch.search.DefinitionIdBase;

public final class DefinitionIdViewModel {
	
	private final DefinitionIdBase definitionId;

	public DefinitionIdViewModel(DefinitionIdBase definitionId) {
		this.definitionId = definitionId;
	}

	public String getId() {
		return definitionId.toString();
	}
	
	public String getLabel() {
		
		if(definitionId.hasLabel())
			return definitionId.getLabel();
		
		return definitionId.getName();
	}
	
	@Override
	public int hashCode() {
		return definitionId.getHashCode();
	}
	
	@Override
	public boolean equals(Object obj) {

		if (!(obj instanceof DefinitionIdViewModel)) 
			return false;
 
		return definitionId.equals(((DefinitionIdViewModel) obj).definitionId);		
	}

	DefinitionIdBase getDefinitionId() {
		return definitionId;
	}
}
