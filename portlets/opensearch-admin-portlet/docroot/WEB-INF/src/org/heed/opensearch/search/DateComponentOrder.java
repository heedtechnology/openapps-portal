package org.heed.opensearch.search;

public enum DateComponentOrder {
    BigEndian, SmallEndian, MiddleEndian
}
