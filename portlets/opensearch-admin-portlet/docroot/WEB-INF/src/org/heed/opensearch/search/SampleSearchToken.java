package org.heed.opensearch.search;

import org.codehaus.jparsec.Token;
import org.codehaus.jparsec.Tokens.Fragment;

public class SampleSearchToken implements Comparable<SampleSearchToken>  {
	private final Token[] tokenArray;
	private final int index;
	private int type;
	
	public static final int TYPE_KNOWN = 1;
	public static final int TYPE_UNKNOWN = 2;
	
	public SampleSearchToken(Token[] tokenArray, int index) {
		this.tokenArray = tokenArray;
		this.index = index;
	}

	public String value() {
		return ((Fragment) tokenArray[this.index].value()).text();
	}

	@Override
	public int compareTo(SampleSearchToken o) {
		return o == null ? 1 : Integer.compare(index, o.index()) ;
	}

	public int index() {
		return index;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
}
