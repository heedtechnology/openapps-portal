package org.heed.opensearch.search;

import java.util.Set;

import org.codehaus.jparsec.Parser;
import org.codehaus.jparsec.Scanners;
import org.codehaus.jparsec.pattern.CharPredicate;
import org.codehaus.jparsec.pattern.CharPredicates;
import org.codehaus.jparsec.pattern.Pattern;

/**
 * Created with IntelliJ IDEA. User: Babar Date: 10/23/13 Time: 5:53 AM To
 * change this template use File | Settings | File Templates.
 */
public class DefinitionParserFactory {

	private final Set<String> definitionNames;
	private final LexNode root;

	public DefinitionParserFactory(Set<String> defintionNames) {

		root = new LexNode();
		this.definitionNames = defintionNames;

		for (String def : defintionNames)
			loadDefinition(def, root);
	}

	private void loadDefinition(String def, LexNode root) {

		LexNode node = root;
		loadDefintion(def, node, false, def);
	}

	private void loadDefintion(String def, LexNode node,
			boolean isCaseSensitive, String keyword) {

		CharSequence charSeq = keyword;

		for (int i = 0; i < charSeq.length(); i++) {

			char ch = charSeq.charAt(i);
			LexNode n = node.getChild(ch);
			if (n != null)
				node = n;
			else {
				n = new LexNode(ch, node, isCaseSensitive);
				if (n != null)
					node = n;
			}
			if (i == charSeq.length() - 1)
				n.addDefinition(def);
		}
	}

	public Parser<Void> CreateParserInstance() {
		return Scanners.pattern(new DefintionPattern(root), "definition");
	}

	private static class DefintionPattern extends Pattern {

		private final LexNode root;
		private static final CharPredicate isWhitespace = CharPredicates.IS_WHITESPACE;

		public DefintionPattern(LexNode root) {
			this.root = root;
		}

		@Override
		public int match(CharSequence src, int begin, int end) {

			int definitionCharIndex = MISMATCH, lastMatchedCharIndex = MISMATCH;
			LexNode node = root;

			for (int i = begin; i < end; i++) {

				char ch = src.charAt(i);

				if (node.childExists(ch)
						|| node.childExists(Character.toUpperCase(ch))
						|| node.childExists(Character.toLowerCase(ch))) {

					node = node.getChild(ch);
					lastMatchedCharIndex = i;

				} else if (isWhitespace.isChar(ch)) {

					if (node.hasDefinition())
						definitionCharIndex = lastMatchedCharIndex;

				} else
					break;
			}

			if (definitionCharIndex == MISMATCH) {

				if (lastMatchedCharIndex == MISMATCH)
					return MISMATCH;

				if (!node.hasDefinition() || lastMatchedCharIndex != end-1)
					return MISMATCH;

				definitionCharIndex = lastMatchedCharIndex;
			}

			return (definitionCharIndex - begin) + 1;
		}
	}
}
