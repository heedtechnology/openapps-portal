package org.heed.opensearch.search;


class MonthPattern extends AbstractMonthPattern {

	@Override
	public int match(CharSequence src, int begin, int end) {
		return matchMonthNumericPortion(src, begin, end);
	}
}
