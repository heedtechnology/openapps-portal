package org.heed.opensearch.portal.search.viewmodel;

import org.heed.opensearch.search.SampleSearchParser;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.vaadin.data.util.BeanItemContainer;

@SuppressWarnings("serial")
public final class SampleSearchesViewModel extends BeanItemContainer<SampleSearchViewModel> {
	private final String initialSearch = "";
	private String newSearch = initialSearch;
	
	private final SampleSearchParser sampleSearchParser;
	
	private final NewSampleSearchViewModel newSampleSearchViewModel = new NewSampleSearchViewModel(this);
	
	public SampleSearchesViewModel() {
		
		super(SampleSearchViewModel.class);
		
		SampleSearchParser sampleSearchParser = null;
		try {
			sampleSearchParser = new SampleSearchParser();
		} catch (OWLOntologyCreationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.sampleSearchParser = sampleSearchParser;
	}

	String getNewSearch() {
		return newSearch;
	}

	void setNewSearch(String newValue) {
		newSearch =newValue;
	}
	
	public void makeNewSampleSearchAvailableToUser(){
		addBean(new SampleSearchViewModel(sampleSearchParser.parse(newSearch)));
		newSearch = initialSearch;
	}

	public NewSampleSearchViewModel getNewSampleSearchViewModel() {
		return newSampleSearchViewModel;
	}
}
