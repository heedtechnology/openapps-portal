package org.heed.opensearch.search;

import org.codehaus.jparsec.pattern.Pattern;

public abstract class AbstractMonthPattern extends Pattern {

	public AbstractMonthPattern() {
		super();
	}

	public abstract int match(CharSequence src, int begin, int end);

	protected static int matchMonthNumericPortion(CharSequence src, int begin, int end) {
		int length = end - begin;
	
	    if(length < 1)
	        return MISMATCH;
	
	    if(length >= 2) {
	        String string = src.subSequence(begin, begin + 2).toString();
	
	        switch (string){
	            case "01" :case "02" :case "03" : case "04" :case "05" :case "06" :case "07" :case "08" :case "09" :
	            case "10" :case "11" :case "12":
	                return 2;
	        }
	    } else {
	        String string = src.subSequence(begin, begin + 1).toString();
	
	        switch (string) {
	            case "1":
	            case "2":
	            case "3":
	            case "4":
	            case "5":
	            case "6":
	            case "7":
	            case "8":
	            case "9":
	                return 1;
	        }
	    }
	
	    return MISMATCH;
	}
}