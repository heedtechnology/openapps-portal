package org.heed.opensearch.search;

import java.io.File;
import java.io.StringWriter;
import java.util.Set;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.util.ShortFormProvider;
import org.semanticweb.owlapi.util.SimpleShortFormProvider;

import com.liferay.portal.kernel.util.PropsUtil;

import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxFrameRenderer;

public final class PizzaDictionaryProvider {	
	
    private static final String HOME_DIR = PropsUtil.get("home.dir") ;
	static final String DATA_DIR = HOME_DIR + "/resources/data/";
    static final String ONTOLOGIES_DIR = HOME_DIR + "/resources/ontologies/";
    static final String PIZZA_ONTOLOGY_FILE = ONTOLOGIES_DIR + "pizza.owl";
    
    public static Dictionary<ClassDefintion> provideDictionaryOfClassDefinition() throws OWLOntologyCreationException {
    	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    	OWLDataFactory dataFactory = OWLManager.getOWLDataFactory();
    	
    	File file = new File(PIZZA_ONTOLOGY_FILE);
    	
        OWLOntology pizzaOntology = manager.loadOntologyFromOntologyDocument(file);
        
        final Set<OWLClass> classes = pizzaOntology.getClassesInSignature();
        
        Dictionary<ClassDefintion> dictionary = new Dictionary<ClassDefintion>();
        
        for (OWLClass clazz : classes) {
        	final Set<OWLAnnotation> labelAnnotations = clazz.getAnnotations(pizzaOntology, dataFactory.getRDFSLabel());
        	
        	String label = null;
        	for (OWLAnnotation owlAnnotation : labelAnnotations) {
				final OWLLiteral labelValue = (OWLLiteral) owlAnnotation.getValue();
				if (labelValue.hasLang("en")) {
        			label = labelValue.getLiteral();
        			break;
        		}
			}
        	
        	dictionary.addDefinition(new ClassDefintion(clazz, label, new Renderar(pizzaOntology)));
		}
        
    	return dictionary;
    }

    public static Dictionary<PropertyDefinition<?,?>> provideDictionaryOfPropertyDefinition() throws OWLOntologyCreationException {
    	OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    	OWLDataFactory dataFactory = OWLManager.getOWLDataFactory();
    	
    	File file = new File(PIZZA_ONTOLOGY_FILE);
    	
        OWLOntology pizzaOntology = manager.loadOntologyFromOntologyDocument(file);
        
        Dictionary<PropertyDefinition<?,?>> dictionary = new Dictionary<PropertyDefinition<?,?>>();
        
        loadProperties(dataFactory, pizzaOntology.getDataPropertiesInSignature(), pizzaOntology, dictionary);
        loadProperties(dataFactory, pizzaOntology.getObjectPropertiesInSignature(), pizzaOntology, dictionary);
        
        return dictionary;
    }

	@SuppressWarnings("rawtypes")
	private static void loadProperties(OWLDataFactory dataFactory, final Set<? extends OWLProperty<?, ?>> properties,
			OWLOntology ontology,  Dictionary<PropertyDefinition<?, ?>> dictionary) {
		 
        for (OWLProperty<?, ?> property : properties) {
			final Set<OWLAnnotation> labelAnnotations = property.getAnnotations(ontology, dataFactory.getRDFSLabel());
		
			String label =  null;
			for (OWLAnnotation owlAnnotation : labelAnnotations) {
				final OWLLiteral labelValue = (OWLLiteral)owlAnnotation.getValue();
				if (labelValue.hasLang("en")) {
					label = labelValue.getLiteral();
					break;
				}
			}
			
			dictionary.addDefinition(new PropertyDefinition(property, label, new Renderar(ontology)));
		}
	}
	
	private static class Renderar implements OWLObjectRenderer {
		private final OWLOntology ontology;

		public Renderar(OWLOntology ontolgy) {
			this.ontology = ontolgy;
		}
		
		@Override
		public String render(OWLEntity owlEntity) {
			StringWriter writer = new StringWriter();
			ShortFormProvider shortNameProvider = new SimpleShortFormProvider();
			final ManchesterOWLSyntaxFrameRenderer renderer = new ManchesterOWLSyntaxFrameRenderer(ontology, writer, shortNameProvider);
			renderer.writeFrame(owlEntity);
			return writer.toString();
		}
		
	}
}
