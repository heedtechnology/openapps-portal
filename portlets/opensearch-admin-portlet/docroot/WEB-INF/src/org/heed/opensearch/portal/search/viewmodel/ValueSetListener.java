package org.heed.opensearch.portal.search.viewmodel;

public interface ValueSetListener<ValueType> {
	
	void valueSet(ObservableProperty<ValueType> property, ValueType currentValue, ValueType newValue);

}
