package org.heed.opensearch.search;

import org.codehaus.jparsec.*;
import org.codehaus.jparsec.functors.*;
import org.codehaus.jparsec.functors.Map;
import org.codehaus.jparsec.pattern.CharPredicates;
import org.codehaus.jparsec.pattern.Pattern;
import org.codehaus.jparsec.pattern.Patterns;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.*;

class DateScannersFactory {
	static final String[] monthsArray = new String[] { "january", "february",
			"march", "april", "may", "june", "july", "august", "september",
			"october", "november", "december" };

	static final String[] months = addArrays(monthsArray,
			monthAbbreviation(monthsArray));

	static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMddyy");

	static final String[] monthDaySuffixArray = new String[] { "st", "nd",
			"rd", "th" };

	static final String[] weekdays = new String[] { "monday", "tuesday",
			"wednesday", "thursday", "friday", "saturday", "sunday" };
	static final String[] emptyStringArray = new String[0];

	static final String[] dateQueryKeywords = new String[] { "today",
			"yesterday", "days", "week", "weeks", "month", "months", "year",
			"years", "ago", "last", "next", "before", "after", "prior", "to",
			"between", "and" };

	private static final MonthDayPattern monthDayPattern = new MonthDayPattern();

	private static final String YEAR = "yearFragment";

	private static YearPattern yearPattern = new YearPattern();

	private final static Pattern textYearPattern = Patterns.sequence(Patterns.isChar(CharPredicates.IS_DIGIT),
			Patterns.isChar(CharPredicates.IS_DIGIT),Patterns.isChar(CharPredicates.IS_DIGIT),Patterns.isChar(CharPredicates.IS_DIGIT));

	static final TextMonthDayPattern textMonthDayPattern = new TextMonthDayPattern() ;

	private static final String MONTH = "monthPattern";
	
	private static final String TEXT_MONTH = "textMonthPattern";

	private final static MonthPattern monthPattern = new MonthPattern();

	public static final String MONTH_DAY = "monthDayFragment";

	private static final String TEXT_MONTH_DAY = "textMonthDayFragment";

	private static final String WEEKDAY = "weekdayFragment";

	private static final String TEXT_YEAR = "textYearFragment";
		
	static Parser<Void> createDateScanner() {
		return createNumericDateScanner().or(createTextDateScanner());
	}

	static Parser<Void> createTextDateScanner() {
		
		final Parser<Void> yearScanner = Scanners.pattern(textYearPattern, TEXT_YEAR);
		final Pattern[]  monthPatternArray = new Pattern[months.length];
		final Pattern[] weekDayPatternArray = new Pattern[weekdays.length];
		
		for (int i = 0; i < monthPatternArray.length; i++) {
			monthPatternArray[i] = Patterns.string(months[i]);
		}
		
		for (int i = 0; i < weekDayPatternArray.length; i++) {
			weekDayPatternArray[i] = Patterns.string(weekdays[i]);
		}
		
		final Parser<Void> monthScanner = Scanners.pattern(Patterns.or(monthPatternArray), TEXT_MONTH);
		final Parser<Void> monthDayScanner = Scanners.pattern(textMonthDayPattern, TEXT_MONTH_DAY); 
		final Parser<Void> weekDayScanner = Scanners.pattern(Patterns.or(weekDayPatternArray), WEEKDAY);
				
		final Parser<Void> dmyDate = sequence(sequence(weekDayScanner, Scanners.string(", ")).optional(), monthDayScanner,
				Scanners.string(" "), monthScanner, sequence(Scanners.string(" "), yearScanner).optional());
		
		final Parser<Void> theDayOfMonthYearDate = sequence(weekDayScanner.optional(), Scanners.string("the "), monthDayScanner,
				Scanners.string(" of "), monthScanner, Scanners.string(",").optional(), Scanners.string(" "), yearScanner,
				Scanners.string(",").optional());
		
		final Parser<Void> mdyDate = sequence(sequence(weekDayScanner, Scanners.string(", ")).optional(), monthScanner,
				Scanners.string(" the").optional(), Scanners.string(" "), monthDayScanner,Scanners.isChar(',').optional(),
				Scanners.string(" "), yearScanner);
		
		return Parsers.or(dmyDate,theDayOfMonthYearDate, mdyDate);
	}
	
	@SafeVarargs
	static Parser<Void> sequence(final Parser<Void>... parser) {
		return sequence(Arrays.asList(parser));
	}
	
	static Parser<Void> sequence(final List<Parser<Void>> parserList) {
		
		final int size = parserList.size();
		if(size > 1)
			return Parsers.sequence(parserList.get(0), sequence(parserList.subList(1, size)));
		
		return parserList.get(0);
	}

	static Parser<Void> createNumericDateScanner() {
		
		final Parser<Void> yearScanner = Scanners.pattern(yearPattern, YEAR);
		final Parser<Void> monthScanner = Scanners.pattern(monthPattern, MONTH);
		final Parser<Void> monthDayScanner = Scanners.pattern(monthDayPattern, MONTH_DAY);
		
		final char[] dateDelimeters = new char[] {',', '/', '\\', ':', '.', '-'};
		
		return Parsers.or(new Iterable<Parser<Void>>() {

			@Override
			public Iterator<Parser<Void>> iterator() {
				List<Parser<Void>> dateParsers =  Arrays.asList(
					Parsers.sequence(yearScanner,monthScanner, monthDayScanner),		
					Parsers.sequence(monthDayScanner,monthScanner,yearScanner),
					Parsers.sequence(monthScanner,monthDayScanner,yearScanner)
				);
				
				for (char c : dateDelimeters) {
					dateParsers.add(Parsers.sequence(yearScanner,Scanners.isChar(c),monthScanner,Scanners.isChar(c), monthDayScanner));
					dateParsers.add(Parsers.sequence(monthDayScanner,Scanners.isChar(c),monthScanner,Scanners.isChar(c),yearScanner));
					dateParsers.add(Parsers.sequence(monthScanner,Scanners.isChar(c),monthDayScanner,Scanners.isChar(c),yearScanner));
				}
				
				dateParsers.add(Parsers.sequence(monthDayScanner, Scanners.isChar('\\'), yearScanner));
				
				return dateParsers.iterator();
			}
		});
	}
	
	private static String[] monthAbbreviation(String[] monthsArray) {
		String[] abbreviations = new String[monthsArray.length];

		for (int i = 0; i < monthsArray.length; i++)
			abbreviations[i] = monthsArray[i].substring(0, 3);

		return abbreviations;
	}

	static String[] addArrays(String[]... stringArrays) {

		int length = 0;

		for (String[] stringArray : stringArrays)
			length += stringArray.length;

		String[] result = new String[length];

		length = 0;

		for (String[] stringArray : stringArrays) {
			System.arraycopy(stringArray, 0, result, length, stringArray.length);
			length += stringArray.length;
		}

		return result;
	}

	public static String lookUpOrdinalNumberSuffix(int number) {

		final String[] suffixArray = new String[] {"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th" };

		switch (number % 100) {
			case 11:
			case 12:
			case 13:
				return "th";
			default:
				return suffixArray[number % 10];
		}
	}
}
