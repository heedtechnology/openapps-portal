package org.heed.opensearch.portal.search.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.heed.opensearch.search.SampleSearch;
import org.heed.opensearch.search.SampleSearchToken;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.themes.BaseTheme;

public class SampleSearchViewModel {
	private final SampleSearch value;

	public SampleSearchViewModel(SampleSearch value) {
		this.value = value;
	}
	
	public List<Component> getValue() {
		List<Component> values = new ArrayList<Component>();
		
		for (SampleSearchToken sampleSearchToken : value.getPhrases()) {
			if(sampleSearchToken.getType() == SampleSearchToken.TYPE_KNOWN) {
				Label label = new Label("["+sampleSearchToken.value()+"]", ContentMode.HTML);
                label.setSizeUndefined();
                values.add(label);
			} else {
				Button link = new Button(sampleSearchToken.value());                           
				link.setStyleName(BaseTheme.BUTTON_LINK);
				link.addClickListener(new Button.ClickListener() {
				    private static final long serialVersionUID = 2591000425251425657L;
					public void buttonClick(ClickEvent event) {
				        Notification.show("Do not press this button again");
				    }
				});
				values.add(link);
			}
		}

		return values;
	}
}
