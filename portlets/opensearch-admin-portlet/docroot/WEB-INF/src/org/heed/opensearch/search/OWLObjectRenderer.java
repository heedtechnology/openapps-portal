package org.heed.opensearch.search;

import org.semanticweb.owlapi.model.OWLEntity;

interface OWLObjectRenderer {
	String render(OWLEntity owlEntity);
}
