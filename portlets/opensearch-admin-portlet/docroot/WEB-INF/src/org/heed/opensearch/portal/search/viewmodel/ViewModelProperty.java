package org.heed.opensearch.portal.search.viewmodel;

public final class ViewModelProperty<ValueType> extends
		ObservableProperty<ValueType> implements ValueSetListener<ValueType> {

	private ObservableProperty<ValueType> observableProperty;

	public ViewModelProperty(ValueType value) {
		super(value);
	}

	public ViewModelProperty(ValueType value,
			ObservableProperty<ValueType> observableProperty) {

		super(value);
		setObservableProperty(observableProperty);
	}

	public void setObservableProperty(
			ObservableProperty<ValueType> observableProperty) {
		this.observableProperty = observableProperty;
		this.observableProperty.addValueSetListener(this);
	}

	@Override
	public void valueSet(ObservableProperty<ValueType> property,
			ValueType currentValue, ValueType newValue) {
		setValue(newValue);
	}
	
	@Override
	protected boolean OnBeforeSet(ValueType currentValue, ValueType newValue) {
		if (equals(this, newValue))
			return false;
		return true;
	}
	
	@Override
	protected void OnAfterSet() {
		ValueType value = getValue();
		if (observableProperty != null && !equals(observableProperty, value))
			observableProperty.setValue(value);		
	}

	private boolean equals(ObservableProperty<ValueType> property,
			ValueType newValue) {
		ValueType propertyValue = property.getValue();

		if (propertyValue == null && newValue == null)
			return true;

		if (propertyValue != null && propertyValue.equals(newValue))
			return true;

		return false;
	}
}
