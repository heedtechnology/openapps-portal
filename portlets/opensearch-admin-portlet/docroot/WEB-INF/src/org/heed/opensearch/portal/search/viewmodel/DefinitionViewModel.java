package org.heed.opensearch.portal.search.viewmodel;

import org.heed.opensearch.search.Definition;

public abstract class DefinitionViewModel<DefinitionType extends Definition> {
	private final DefinitionType definition;

	public DefinitionViewModel(DefinitionType definition) {
		this.definition = definition;
	}

	DefinitionType getDefintion() {
		return definition;
	}

	public String toString() {
		return definition.render();
	}
}