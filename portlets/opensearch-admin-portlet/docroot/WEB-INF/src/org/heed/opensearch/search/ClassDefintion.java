package org.heed.opensearch.search;

import org.semanticweb.owlapi.model.OWLClass;

public final class ClassDefintion extends Definition<NamedObjectId> {
	private final OWLClass owlClass;
	private final OWLObjectRenderer renderer;
	
	ClassDefintion(OWLClass owlClass, String label, OWLObjectRenderer renderer) {
		super(new NamedObjectId(owlClass, label));
		this.renderer = renderer;
		this.owlClass = owlClass;
	}
	
	public OWLClass getOwlClass(){
		return owlClass;
	}
	
	@Override
	public String render() {
		return renderer.render(owlClass);
	}
}	