package org.heed.opensearch.search;

import org.semanticweb.owlapi.model.OWLProperty;
import org.semanticweb.owlapi.model.OWLPropertyExpression;
import org.semanticweb.owlapi.model.OWLPropertyRange;

import uk.ac.manchester.cs.owl.owlapi.mansyntaxrenderer.ManchesterOWLSyntaxFrameRenderer;

public final class PropertyDefinition<R extends OWLPropertyRange, P extends OWLPropertyExpression<R, P>> extends Definition<NamedObjectId> {
	private final  OWLProperty<R, P> owlProperty;
	private final OWLObjectRenderer renderer;

	public PropertyDefinition(OWLProperty<R, P> owlProperty, String label, OWLObjectRenderer renderer){
		super(new NamedObjectId(owlProperty, label));
		this.owlProperty = owlProperty;
		this.renderer = renderer;
	}

	public OWLProperty<R, P> getProperty() {
		return owlProperty;
	}

	@Override
	public String render() {
		return renderer.render(owlProperty);
	}
}
