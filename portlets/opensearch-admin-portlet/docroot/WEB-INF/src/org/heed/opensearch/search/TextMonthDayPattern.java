package org.heed.opensearch.search;

public class TextMonthDayPattern extends AbstractMonthPattern {

	@Override
	public int match(CharSequence src, int begin, int end) {
		final int matchedLength = matchMonthNumericPortion(src, begin, end);
		
		if(matchedLength == MISMATCH)
			return MISMATCH;
		
		final int month = Integer.parseInt(src.subSequence(begin, begin + matchedLength).toString());
		final String suffix = DateScannersFactory.lookUpOrdinalNumberSuffix(month);
		final int matchedLengthWithSuffix = matchedLength + suffix.length();
		
		if((end - begin) < matchedLengthWithSuffix)
			return matchedLength;
		
		String monthSuffix = src.subSequence(begin + matchedLength, begin + matchedLengthWithSuffix).toString();
		
		if(monthSuffix.equalsIgnoreCase(suffix))
			return matchedLengthWithSuffix;
		
		return matchedLength;
	}
}
