package org.heed.opensearch.search;

import org.semanticweb.owlapi.model.OWLNamedObject;
import org.semanticweb.owlapi.util.SimpleIRIShortFormProvider;

public final class NamedObjectId extends DefinitionIdBase {
	private final OWLNamedObject namedObject;
	private final String label;
	private static final SimpleIRIShortFormProvider simpleIRIShortFormProvider = new SimpleIRIShortFormProvider();

	public NamedObjectId(OWLNamedObject namedObject, String label) {
		super();
		this.namedObject = namedObject;
		this.label = label;
	}
	
	
	@Override
	public String toString() {
		return namedObject.getIRI().toString();
	}

	@Override
	public String getName() {
		return simpleIRIShortFormProvider.getShortForm(namedObject.getIRI());
	}
	
	@Override
	public String getLabel() {
		
		if(!hasLabel())
			throw new UnsupportedOperationException();
		
		return label;
	}

	@Override
	public boolean hasLabel() {
		return label != null;
	}
	
	@Override
	public int getHashCode() {
		return namedObject.getIRI().hashCode();
	}
	
	@Override
	public boolean equals(DefinitionIdBase definitionId) {

		if (! (definitionId instanceof NamedObjectId)) 
			return false;
		
		return ((NamedObjectId) definitionId).namedObject.getIRI().equals(namedObject.getIRI());
	}
}