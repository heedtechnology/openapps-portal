package org.heed.opensearch.search;

import org.codehaus.jparsec.pattern.Pattern;
import org.codehaus.jparsec.pattern.Patterns;

class YearPattern extends Pattern {

	@Override
	public int match(CharSequence src, int begin, int end) {
		return Patterns.INTEGER.match(src, begin, end);
	}

}
