package org.heed.opensearch.search;

public abstract class Definition<DefinitionIdType extends DefinitionIdBase> {
	private final DefinitionIdType id;

	Definition(DefinitionIdType id) {
		this.id = id;
	}

	public DefinitionIdType getId() {
		return id;
	}
	
	public abstract String render();
}