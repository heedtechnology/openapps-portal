<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<c:if test="${result.contentType == 'video'}">
	<link href="http://vjs.zencdn.net/c/video-js.css" rel="stylesheet">
	<script src="http://vjs.zencdn.net/c/video.js"></script>
</c:if>

<div class="collection-detail">
	<div class="collection-detail-left">
		<div class="collection-detail-title">
			<c:out value="${result.title}" />
		</div>
		<div>
			<div class="collection-detail-label" style="">Description:</div>
			<div class="collection-detail-value">
				<c:out value="${result.description}" escapeXml="false" />
			</div>
		</div>
		
	</div>
</div>
