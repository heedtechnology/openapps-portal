<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<c:if test="${not empty resultset.results}">
	<c:if test="${resultset.start != 0}">
		Results <b><c:out value="${resultset.start}" /> - <c:out value="${resultset.end}" /></b> of <b><c:out value="${resultset.resultCount}" /></b> for <b><c:out value="${resultset.query}" /></b>
	</c:if>
	<c:if test="${resultset.start == 0}">
		Results <b>1 - <c:out value="${resultset.end}" /></b> of <b><c:out value="${resultset.resultCount}" /></b> for <b><c:out value="${resultset.query}" /></b>
	</c:if>
</c:if>
<c:if test="${empty resultset.results}">
	No Results for <b><c:out value="${resultset.lastBreadcrumb.name}" /></b>
</c:if>