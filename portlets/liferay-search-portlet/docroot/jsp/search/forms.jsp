<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
String header = (String)renderRequest.getAttribute("header");
if(header == null) header = "";
%>

<script type="text/javascript">
function loadPage(url) {
	$.ajax({	//create an ajax request to load_page.php
		type: "GET",
	    url: url,
	    dataType: "html",	//expect html to be returned
	    success: function(msg){
	    	if(parseInt(msg)!=0) {
	        	$('#liferay-search-results').html(msg);
	        }
	    }
	});
}
</script>

<div class="support-col2"> 
	<div class="support-panel1"> 
		<div class="support-header"> 
			<div class="support-subtitle1">Forms</div>
			<div class="support-subtitle2">(Left-click to open, right-click and select "Save As" to download)</div>
		</div> 
		<div class="support-body"> 
			<c:forEach items="${resultset.results}" var="result" varStatus="rstatus">							
			<div class="form-entry">
			    <table>
				    <tr>
				        <td style="vertical-align:top">
				            <img style="max-width:none;" src="/hgarc-main-theme/images/file_system/small/pdf.png" />
				        </td>
				        <td style="vertical-align:top">
				            <div class="form-title">
				                <a href="<c:out value="${result.url}" />">
				                	<c:out value="${result.title}" escapeXml="false" />
				                </a>
				            </div>
				            <div class="form-size">(<c:out value="${result.size}" />)</div>				            
				        </td>
				    </tr>
				    <tr>
				    	<td></td>
				    	<td>
				    		<div class="form-descr"><c:out value="${result.description}" escapeXml="false" /></div>
				    	</td>
				    </tr>
		        </table>
			</div>
			</c:forEach>
		</div> 
	</div> 
</div>