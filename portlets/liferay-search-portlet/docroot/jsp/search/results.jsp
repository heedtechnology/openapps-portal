<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
String header = (String)renderRequest.getAttribute("header");
if(header == null) header = "";
%>

<script type="text/javascript">
function loadPage(url) {
	$('#liferay-search-results').load(url + ' #liferay-search-results');
}
</script>

<div id="liferay-search-results">
	<table style="width:100%;">
		<tr>
			<td class="archive-results-summary">
				<%@ include file="include/results_header.jsp" %>
			</td>					
		</tr>
		<tr>
			<td>
				<table id="search-table">
					<tbody>
						<c:forEach items="${resultset.results}" var="result" varStatus="rstatus">
							<tr>						
								<td class="displayDate">
									<c:out value="${result.displayDate}" />									
								</td>
							</tr>
							<tr>
								<td  class="result">
									<div class="title">
										<c:out value="${result.title}" escapeXml="false" />
									</div>									
									<div class="description">
										<c:out value="${result.description}" escapeXml="false" />
									</div>
								</td>
							</tr>
							<tr>
								<td colspan="3">
									<div class="search-line-border"></div>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
	
				<table class="search-tools">
					<tbody><tr>
						<td></td>
						<td class="tools-right">
							<c:forEach items="${resultset.paging}" var="page" varStatus="pstatus">
								<a href="#" onclick="loadPage('<c:out value="${baseUrl}" />?<c:out value="${page.query}" />');">
									<c:out value="${page.name}" />
								</a>
							</c:forEach>	
						</td>
					</tr></tbody>
				</table>
			</td>
		</tr>
	</table>
</div>