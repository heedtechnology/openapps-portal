package org.heed.openapps.portal.domain;

import com.liferay.portal.kernel.repository.model.FileEntry;


public class Document extends Result {
	private String url;
	private String size;
	
	
	public Document(FileEntry entry) {
		setTitle(entry.getTitle());
		setDescription(entry.getDescription());
		
		long size = entry.getSize();
		setSize(Math.round(size / 1000) + "k");
					
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}	
}
