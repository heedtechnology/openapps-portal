package org.heed.openapps.portal.portlet.search;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.heed.openapps.portal.domain.Comment;
import org.heed.openapps.portal.domain.Discussion;
import org.heed.openapps.portal.domain.Result;
import org.heed.openapps.portal.util.MBMessageJSONSerializer;
import org.heed.openapps.portal.util.SubscriptionJSONSerializer;
import org.json.JSONObject;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PrefsParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.Subscription;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextFactory;
import com.liferay.portal.service.SubscriptionLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetLink;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetLinkLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMStructure;
import com.liferay.portlet.dynamicdatamapping.service.DDMStructureLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.messageboards.model.MBCategory;
import com.liferay.portlet.messageboards.model.MBMessage;
import com.liferay.portlet.messageboards.service.MBMessageLocalServiceUtil;
import com.liferay.portlet.messageboards.service.MBThreadLocalServiceUtil;


public class DisplayPortlet extends PortletSupport {
	private static Log log = LogFactoryUtil.getLog(DisplayPortlet.class);
		
	
	@SuppressWarnings("unchecked")
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		String view = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "view", "");
		
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		HttpServletRequest httpReq = PortalUtil.getHttpServletRequest(renderRequest);
		HttpServletRequest httpReq2 = PortalUtil.getOriginalServletRequest(httpReq);
		
		String id = httpReq2.getParameter("id") != null ? httpReq2.getParameter("id") : null;
		
		if(id != null) {
			try {
				if(view.equals("discussion") || view.equals("comment")) {
					MBMessage message = MBMessageLocalServiceUtil.getMBMessage(Long.valueOf(id));
					renderRequest.setAttribute("messageId", String.valueOf(message.getMessageId()));
					if(message.isRoot()) {
						List<MBMessage> messages = MBMessageLocalServiceUtil.getThreadMessages(message.getThreadId(), WorkflowConstants.STATUS_APPROVED);
						Discussion discussion = new Discussion(message);					
						for(MBMessage msg : messages) {
							if(!msg.equals(message)) {
								Comment comment = new Comment(msg, messages);
								discussion.getComments().add(comment);
							}
						}
						List<Long> pageViews = (List<Long>)renderRequest.getPortletSession().getAttribute("pageViews");
						if(pageViews == null) pageViews = new ArrayList<Long>();
						if(!pageViews.contains(message.getThreadId())) {
							//only increment a page once per session per user
							MBThreadLocalServiceUtil.incrementViewCounter(message.getThreadId(), 1);
							pageViews.add(message.getThreadId());
							renderRequest.getPortletSession().setAttribute("pageViews", pageViews);
						}
						
						renderRequest.setAttribute("result", discussion);
					} else {
						MBMessage rootMessage = MBMessageLocalServiceUtil.getMBMessage(message.getRootMessageId());
						Comment comment = new Comment(message, new ArrayList<MBMessage>());
						comment.setCategoryId(rootMessage.getCategoryId());
						comment.setCategoryName(rootMessage.getCategory().getName());
						comment.setCategoryDesc(rootMessage.getCategory().getDescription());
						renderRequest.setAttribute("result", comment);
					}
					long group = message.getCategoryId();
					renderRequest.setAttribute("isGroupMember", isDiscussionGroupMember(renderRequest, String.valueOf(group)));
				} else {
					JournalArticle article = JournalArticleLocalServiceUtil.getLatestArticle(Long.valueOf(id));
					if(article != null) {
						DDMStructure structure = DDMStructureLocalServiceUtil.getStructure(article.getGroupId(), 10109, article.getStructureId());
						if(structure != null) {
							view = structure.getName(themeDisplay.getLocale()).toLowerCase();
						}					
						Result resource = new Result(themeDisplay, article);						
						AssetEntry assetEntry = AssetEntryLocalServiceUtil.getEntry(JournalArticle.class.getName(), article.getResourcePrimKey());
						List<AssetLink> links = AssetLinkLocalServiceUtil.getDirectLinks(assetEntry.getEntryId());
						for(AssetLink link : links) {
							AssetEntry entry = AssetEntryLocalServiceUtil.getEntry(link.getEntryId2());
							JournalArticle relatedArticle = JournalArticleLocalServiceUtil.getLatestArticle(entry.getClassPK());
							Result result = new Result(themeDisplay, relatedArticle);
							resource.getRelatedAssets().add(result);
						}						
						renderRequest.setAttribute("result", resource);							
					}
				}
			} catch(Exception e) {
				throw new IOException(e);
			}
		} else log.info("no article :"+id);
			
		include("/jsp/display/"+view+".jsp", renderRequest, renderResponse);
	}
	
	@SuppressWarnings("deprecation")
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse) throws IOException, PortletException {
		ThemeDisplay themeDisplay = (ThemeDisplay) resourceRequest.getAttribute(WebKeys.THEME_DISPLAY);
		HttpServletRequest httpReq = PortalUtil.getHttpServletRequest(resourceRequest);
		HttpServletRequest httpReq2 = PortalUtil.getOriginalServletRequest(httpReq);
		
		String action = httpReq2.getParameter("action");
		String id = httpReq2.getParameter("id");
		String comment = httpReq2.getParameter("comment");
		
		try {
			ServiceContext serviceContext = ServiceContextFactory.getInstance(MBMessage.class.getName(), resourceRequest);
			serviceContext.setAddCommunityPermissions(true);
			long classPK = 0;
			String className = "";
			long userId = themeDisplay.getUserId();
			String userName = themeDisplay.getUser().getScreenName();
			long groupId = themeDisplay.getScopeGroupId();
			
			if(action.equals("joinGroup")) {
				Subscription sub = SubscriptionLocalServiceUtil.addSubscription(userId, themeDisplay.getScopeGroupId(), MBCategory.class.getName(), Long.valueOf(id), "");
				JSONObject json = SubscriptionJSONSerializer.toJSONObject(sub);
				resourceResponse.getWriter().append(json.toString());
			} else if(action.equals("leaveGroup")) {
				SubscriptionLocalServiceUtil.deleteSubscription(userId, MBCategory.class.getName(), Long.valueOf(id));
				JSONObject json = SubscriptionJSONSerializer.toJSONObject(id);
				resourceResponse.getWriter().append(json.toString());
			} else {
				MBMessage parentMsg = MBMessageLocalServiceUtil.getMBMessage(Long.valueOf(id));
				MBMessage msg = MBMessageLocalServiceUtil.addDiscussionMessage(userId, userName, groupId, className, classPK, parentMsg.getThreadId(), parentMsg.getMessageId(), "", comment, serviceContext);
				JSONObject json = MBMessageJSONSerializer.toJSONObject(msg);
				resourceResponse.getWriter().append(json.toString());
			}
		} catch(Exception e) {
			throw new IOException(e);
		}		
		//includeResource("/jsp/collections.jsp", resourceRequest, resourceResponse);
	}
}
