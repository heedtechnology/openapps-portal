package org.heed.openapps.portal.domain;
import java.util.ArrayList;
import java.util.List;

import org.dom4j.Element;

import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.journal.model.JournalArticle;


public class Result {
	private String id;
	private String title;
	private String description;
	private String contentType;
	private String displayDate;
	private List<Result> relatedAssets = new ArrayList<Result>();
	
	
	public Result() {}
	public Result(Element entity) {
		try {
			if(entity.attribute("id") != null) this.id = entity.attribute("id").getText();			
			if(entity.element("name") != null) this.title = entity.element("name").getText();
			if(entity.element("description") != null) this.description = entity.element("description").getText();
			if(entity.attribute("localName") != null) this.contentType = entity.attribute("localName").getText();
			//if(entity.element("description") != null && !entity.element("description").getTextTrim().equals(this.title)) 
				//notes.add(new Note("", "Description", entity.element("description").getTextTrim()));
			
			if(title != null) title = title.replace("<br>", "");
			if(description != null) description = description.replace("<br>", "");
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public Result(String id, String title) {
		this.id = id;
		this.title = title;
	}	
	public Result(ThemeDisplay themeDisplay, JournalArticle article) {
		this(String.valueOf(article.getPrimaryKey()), article.getTitle(themeDisplay.getLocale()));
		setDescription(article.getDescription());
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getDisplayDate() {
		return displayDate;
	}
	public void setDisplayDate(String displayDate) {
		this.displayDate = displayDate;
	}
	public List<Result> getRelatedAssets() {
		return relatedAssets;
	}
	public void setRelatedAssets(List<Result> relatedAssets) {
		this.relatedAssets = relatedAssets;
	}
}
