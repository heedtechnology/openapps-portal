package org.heed.openapps.portal.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Subscription;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * <a href="MBMessageJSONSerializer.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.liferay.portlet.messageboards.service.http.MBMessageServiceJSON</code>
 * to translate objects.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.liferay.portlet.messageboards.service.http.MBMessageServiceJSON
 *
 */
public class SubscriptionJSONSerializer {
	private static Log log = LogFactoryUtil.getLog(SubscriptionJSONSerializer.class);
	
	
	public static JSONObject toJSONObject(String id) {
	    JSONObject jsonObj = new JSONObject();
	    put(jsonObj, "subscriptionId", id);	    
	    return jsonObj;
	}
	
	public static JSONObject toJSONObject(Subscription model) {
	    JSONObject jsonObj = new JSONObject();	
	    //put(jsonObj, "uuid", model.getUuid());
	    put(jsonObj, "subscriptionId", model.getSubscriptionId());
	    put(jsonObj, "companyId", model.getCompanyId());
	    put(jsonObj, "userId", model.getUserId());
	    put(jsonObj, "userName", model.getUserName());
	    put(jsonObj, "createDate", model.getCreateDate());
	    put(jsonObj, "modifiedDate", model.getModifiedDate());
	    put(jsonObj, "classPK", model.getClassPK());
	    return jsonObj;
	}

	@SuppressWarnings("rawtypes")
	public static JSONArray toJSONArray(List models) {
	    JSONArray jsonArray = new JSONArray();
	
	    for (int i = 0; i < models.size(); i++) {
	    	Subscription model = (Subscription)models.get(i);
	
	      jsonArray.put(toJSONObject(model));
	    }
	
	    return jsonArray;
	}
	protected static void put(JSONObject jsonObj, String key, Object value) {
		try {
			jsonObj.put(key, value);
		} catch(Exception e) {
			log.error("", e);
		}
	}
}