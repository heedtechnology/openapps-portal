package org.heed.openapps.portal.portlet.search;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.heed.openapps.portal.domain.Document;
import org.heed.openapps.portal.domain.Paging;
import org.heed.openapps.portal.domain.Result;
import org.heed.openapps.portal.domain.ResultSet;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.search.BooleanClauseOccur;
import com.liferay.portal.kernel.search.BooleanQuery;
import com.liferay.portal.kernel.search.BooleanQueryFactoryUtil;
import com.liferay.portal.kernel.search.Field;
import com.liferay.portal.kernel.search.Hits;
import com.liferay.portal.kernel.search.SearchContext;
import com.liferay.portal.kernel.search.SearchEngineUtil;
import com.liferay.portal.kernel.search.Sort;
import com.liferay.portal.kernel.search.SortFactoryUtil;
import com.liferay.portal.kernel.util.PrefsParamUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMStructure;
import com.liferay.portlet.dynamicdatamapping.service.DDMStructureLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;


public class LiferaySearchPortlet extends PortletSupport {
	private static Log log = LogFactoryUtil.getLog(LiferaySearchPortlet.class);
	
	
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		String structures = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "structures", null);
		String fields = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "fields", "");
		String view = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "view", "results");
		String sort = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "sort", "");
		
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String namespace = PortalUtil.getPortletNamespace(PortalUtil.getPortletId(renderRequest));
		
		HttpServletRequest httpReq = PortalUtil.getHttpServletRequest(renderRequest);
		HttpServletRequest httpReq2 = PortalUtil.getOriginalServletRequest(httpReq);
		String query = httpReq2.getParameter("query") != null ? httpReq2.getParameter("query") : null;
		int page = httpReq2.getParameter(namespace+"page") != null ? Integer.valueOf(httpReq2.getParameter(namespace+"page")) : 1;
		int size = httpReq2.getParameter("size") != null ? Integer.valueOf(httpReq2.getParameter("size")) : 5;
				
		String baseUrl = themeDisplay.getURLCurrent();
		if(baseUrl.contains("?")) baseUrl = baseUrl.substring(0, baseUrl.indexOf("?"));
		if(baseUrl.contains(";jsessionid=")) baseUrl.substring(0, baseUrl.indexOf(";jsessionid="));
			
		renderRequest.setAttribute("baseUrl", baseUrl);
		renderRequest.setAttribute("query", query);
			
		ResultSet results = new ResultSet();
		
		int start = (size * page) - size;
		int end = (size * page);
		
		if(view.equals("forms.jsp")) {
			List<Document> liferayResults = getLiferayDocumentResults(themeDisplay, structures, fields, query, start, end, sort);
			if(liferayResults != null && liferayResults.size() > 0) {
				results.getResults().addAll(liferayResults);
				results.setResultCount(results.getResults().size());
			}
		} else {			
			List<Result> liferayResults = getLiferayWebContentResults(themeDisplay, structures, fields, query, start, end, sort, results);
			if(liferayResults != null && liferayResults.size() > 0) {
				results.getResults().addAll(liferayResults);
			}
		}
		
		if(results != null) {
			
			int currentPage = 1;
			if(size > 0) currentPage = end / size;
			int pageCount = 0;
			
			if(size == 0 || results.getResultCount() < size) pageCount = 1;
			else {
				double ratio = (double)results.getResultCount() / size;
				pageCount = (int)(Math.ceil(ratio));
			}
			results.setPageSize(size);
			results.setPageCount(pageCount);
			results.setPage(currentPage);
			
			results.setStart(start);
			if(end > results.getResultCount()) 
				results.setEnd(results.getResultCount());
			else 
				results.setEnd(end);
								
			results.setQuery(query);
							
			if(results.getPage() > 1) {
				String pageQuery = "query="+query+"&"+namespace+"page="+(page - 1);
				if(results.getPageSize() != 10) pageQuery += "&size="+results.getPageSize();
				if(results.getSort() != null) pageQuery += "&sort="+results.getSort();
				results.getPaging().add(new Paging("Previous", pageQuery));
			}
			
			for(int i=1; i < 10; i++) {
				if(results.getPageCount() > page + i) {
					String pageQuery = "query="+query+"&"+namespace+"page="+(page + i);
					if(results.getPageSize() != 10) pageQuery += "&size="+results.getPageSize();
					if(results.getSort() != null) pageQuery += "&sort="+results.getSort();
					results.getPaging().add(new org.heed.openapps.portal.domain.Paging(String.valueOf(results.getPage() + i), pageQuery));
				}
			}
			if(results.getPageCount() > page) {
				String pageQuery = "query="+query+"&"+namespace+"page="+(page + 1);
				if(results.getPageSize() != 10) pageQuery += "&size="+results.getPageSize();
				if(results.getSort() != null) pageQuery += "&sort="+results.getSort();
				results.getPaging().add(new Paging("Next", pageQuery));
			}
			
			log.info(results.getResultCount()+" result returned for query:"+query);
			renderRequest.setAttribute("resultset", results);
			PortletSession session = renderRequest.getPortletSession();
			session.setAttribute("lr_results", results, PortletSession.APPLICATION_SCOPE);
		}
		
		include("/jsp/search/"+view+".jsp", renderRequest, renderResponse);
	}
	
	protected List<Document> getLiferayDocumentResults(ThemeDisplay themeDisplay, String structures, String fields, String query, int start, int end, String sort) throws PortletException {
		List<Document> results = new ArrayList<Document>();
		
		SearchContext searchContext = new SearchContext();			
		try {
			BooleanQuery searchQuery = BooleanQueryFactoryUtil.create(searchContext);				
			if(structures != null && structures.length() > 0) {				
				BooleanQuery structureQuery = BooleanQueryFactoryUtil.create(searchContext);
				String[] ids = structures.split(",");
				for(String id : ids) {
					structureQuery.addTerm("classTypeId", id);
				}
				searchQuery.add(structureQuery, BooleanClauseOccur.MUST);
				
			}
			if(query != null && query.length() > 0) {
				BooleanQuery queryQuery = BooleanQueryFactoryUtil.create(searchContext);
				if(fields != null) {
					String[] fieldNames = fields.split(",");
					for(String field : fieldNames) {
						queryQuery.addTerm(field, query);
					}
				} else queryQuery.addTerm("content", query);
				searchQuery.add(queryQuery, BooleanClauseOccur.MUST);
			}
			searchQuery.addRequiredTerm(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
						    
		    Sort[] querySorts = { SortFactoryUtil.getSort(JournalArticle.class, "expando/custom_fields/Order", "desc") };
			searchContext.setSorts(querySorts);
				
			log.info(searchQuery.toString());
			
		    Hits hits = SearchEngineUtil.search(searchContext.getSearchEngineId(), themeDisplay.getCompanyId(), searchQuery, querySorts, QueryUtil.ALL_POS, QueryUtil.ALL_POS);
			
		    com.liferay.portal.kernel.search.Document[] docs = hits.getDocs();
			if (docs != null) {
				for(int i = 0; i < docs.length; i++) {					
					com.liferay.portal.kernel.search.Document doc = docs[i];
					String entryClassPK = doc.get("entryClassPK");
					FileEntry entry = DLAppLocalServiceUtil.getFileEntry(Long.valueOf(entryClassPK));
					
					Document result = new Document(entry);					
					String fileUrl = themeDisplay.getPortalURL()+"/c/document_library/get_file?uuid="+entry.getUuid()+"&groupId="+themeDisplay.getScopeGroupId();
					result.setUrl(fileUrl);
					
					results.add(result);
				}
			}
		} catch(Exception e) {
			throw new PortletException(e);
		}
		
		return results;
	}
	protected List<Result> getLiferayWebContentResults(ThemeDisplay themeDisplay, String structures, String fields, String query, int start, int end, String sort, ResultSet resultset) throws PortletException {
		List<Result> results = new ArrayList<Result>();
		Map<String,String> contentTypes = new HashMap<String,String>();
		
		SearchContext searchContext = new SearchContext();			
		try {
			BooleanQuery searchQuery = BooleanQueryFactoryUtil.create(searchContext);				
			if(structures != null && structures.length() > 0) {				
				BooleanQuery structureQuery = BooleanQueryFactoryUtil.create(searchContext);
				String[] ids = structures.split(",");
				for(String id : ids) {
					structureQuery.addTerm("classTypeId", id);
					DDMStructure structure = DDMStructureLocalServiceUtil.getDDMStructure(Long.valueOf(id));
					if(structure != null) {
						contentTypes.put(id, structure.getName(themeDisplay.getLocale()));
					}
				}
				searchQuery.add(structureQuery, BooleanClauseOccur.MUST);
				
			}
			if(query != null) {
				BooleanQuery queryQuery = BooleanQueryFactoryUtil.create(searchContext);
				if(fields != null) {
					String[] fieldNames = fields.split(",");
					for(String field : fieldNames) {
						queryQuery.addTerm(field, query);
					}
				} else queryQuery.addTerm("content", query);
				searchQuery.add(queryQuery, BooleanClauseOccur.MUST);
			}
			
			searchQuery.addRequiredTerm(Field.STATUS, WorkflowConstants.STATUS_APPROVED);
			searchQuery.addRequiredTerm("head", "true");
			    
		    Sort[] querySorts = { SortFactoryUtil.create(sort, Sort.LONG_TYPE, false) };
			searchContext.setSorts(querySorts);
				
			//log.info(searchQuery.toString());
			
		    Hits hits = SearchEngineUtil.search(searchContext.getSearchEngineId(), themeDisplay.getCompanyId(), searchQuery, querySorts, start, end);
		    resultset.setResultCount(hits.getLength());
		    
		    com.liferay.portal.kernel.search.Document[] docs = hits.getDocs();
			if (docs != null) {
				log.debug("Docs: " + docs.length);
				for(int i = 0; i < docs.length; i++) {
					Result result = new Result();
					com.liferay.portal.kernel.search.Document doc = docs[i];
					
					String structureKey = doc.get("classTypeId");
					String structureTitle = contentTypes.get(structureKey);
					result.setContentType(structureTitle);					
					
					result.setId(doc.get("rootEntryClassPK"));
					result.setTitle(doc.get("title"));
					result.setDescription(doc.get("ddm/"+structureKey+"/Summary_en_US"));
					result.setDisplayDate(doc.get("ddm/"+structureKey+"/Display_Date_en_US"));
					
					results.add(result);
				}
			}
		} catch(Exception e) {
			throw new PortletException(e);
		}
				
	    return results;
	}
}
