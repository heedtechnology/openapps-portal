package org.heed.openapps.portal.util;
import java.io.StringWriter;

import org.heed.openapps.portal.domain.Discussion;
import org.heed.openapps.portal.domain.DiscussionResultSet;


public class DiscussionRSSGenerator {

	
	public static String generateRSS(DiscussionResultSet resultset) {
		StringWriter out = new StringWriter();
		
		out.append("<rss version=\"2.0\">");
		out.append("<channel>");
		out.append("  <title>"+resultset.getCategoryName()+"</title>"); 
		out.append("  <description>"+resultset.getCategoryDesc()+"</description>");
		out.append("  <language>en-us</language>");
		for(int i=0; i < resultset.getResults().size(); i++) {
			Discussion discussion = (Discussion)resultset.getResults().get(i);
			out.append("  <item>");
			out.append("    <title>"+discussion.getTitle()+"</title>");
			out.append("    <link>/search/discussion?id="+discussion.getId()+"</link>"); 
			out.append("    <description>"+discussion.getDescription()+"</description>");
			out.append("  </item>");
		}
		out.append("</channel>"); 
		out.append("</rss>");
		
		return out.toString();
	}
}
