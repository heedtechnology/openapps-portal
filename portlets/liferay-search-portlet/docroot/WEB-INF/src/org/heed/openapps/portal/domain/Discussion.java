package org.heed.openapps.portal.domain;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.User;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.messageboards.model.MBMessage;


public class Discussion extends Result {
	private static Log log = LogFactoryUtil.getLog(Discussion.class);
	private Date date;
	private long categoryId;
	private String categoryName;
	private String categoryDesc;
	private long user;
	private String username;
	private String avatar;
	private int viewCount;
	private int commentCount;
	private List<Comment> comments = new ArrayList<Comment>();
	
	
	public Discussion(MBMessage msg) {
		super(String.valueOf(msg.getMessageId()), msg.getSubject());
		setDescription(msg.getBody());
		setCategoryId(msg.getCategoryId());
		try {
			
			setCategoryName(msg.getCategory().getName());
			setCategoryDesc(msg.getCategory().getDescription());
			
			setUsername(msg.getUserName());
			setDate(new Date(msg.getCreateDate().getTime()));
			
			User liferayUser = UserLocalServiceUtil.getUser(msg.getUserId());
			String sex = liferayUser.isFemale() ? "female" : "male";
			String avatar =	"/image/user_" + sex + "_portrait?img_id=" + String.valueOf(liferayUser.getPortraitId());
            setAvatar(avatar);
                        
		} catch(Exception e) {
			log.error("", e);
		}
	}
	
	
	public long getUser() {
		return user;
	}
	public void setUser(long user) {
		this.user = user;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	public String getCategoryDesc() {
		return categoryDesc;
	}
	public void setCategoryDesc(String categoryDesc) {
		this.categoryDesc = categoryDesc;
	}
	public int getViewCount() {
		return viewCount;
	}
	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}
	public int getCommentCount() {
		return commentCount;
	}
	public void setCommentCount(int commentCount) {
		this.commentCount = commentCount;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public String getAvatar() {
		return avatar;
	}
	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}
	
}
