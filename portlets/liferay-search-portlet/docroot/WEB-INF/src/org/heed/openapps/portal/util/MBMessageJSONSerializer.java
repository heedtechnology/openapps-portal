package org.heed.openapps.portal.util;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portlet.messageboards.model.MBMessage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * <a href="MBMessageJSONSerializer.java.html"><b><i>View Source</i></b></a>
 *
 * <p>
 * ServiceBuilder generated this class. Modifications in this class will be
 * overwritten the next time is generated.
 * </p>
 *
 * <p>
 * This class is used by
 * <code>com.liferay.portlet.messageboards.service.http.MBMessageServiceJSON</code>
 * to translate objects.
 * </p>
 *
 * @author Brian Wing Shun Chan
 *
 * @see com.liferay.portlet.messageboards.service.http.MBMessageServiceJSON
 *
 */
public class MBMessageJSONSerializer {
	private static Log log = LogFactoryUtil.getLog(MBMessageJSONSerializer.class);
	
	public static JSONObject toJSONObject(MBMessage model) {
	    JSONObject jsonObj = new JSONObject();	
	    put(jsonObj, "uuid", model.getUuid());
	    put(jsonObj, "messageId", model.getMessageId());
	    put(jsonObj, "companyId", model.getCompanyId());
	    put(jsonObj, "userId", model.getUserId());
	    put(jsonObj, "userName", model.getUserName());
	    put(jsonObj, "createDate", model.getCreateDate());
	    put(jsonObj, "modifiedDate", model.getModifiedDate());
	    put(jsonObj, "categoryId", model.getCategoryId());
	    put(jsonObj, "threadId", model.getThreadId());
	    put(jsonObj, "parentMessageId", model.getParentMessageId());
	    put(jsonObj, "subject", model.getSubject());
	    put(jsonObj, "body", model.getBody());
	    //put(jsonObj, "attachments", model.getAttachments());
	    put(jsonObj, "anonymous", model.getAnonymous());
	
	    return jsonObj;
	}

	public static JSONArray toJSONArray(List models) {
	    JSONArray jsonArray = new JSONArray();
	
	    for (int i = 0; i < models.size(); i++) {
	      MBMessage model = (MBMessage)models.get(i);
	
	      jsonArray.put(toJSONObject(model));
	    }
	
	    return jsonArray;
	}
	protected static void put(JSONObject jsonObj, String key, Object value) {
		try {
			jsonObj.put(key, value);
		} catch(Exception e) {
			log.error("", e);
		}
	}
}