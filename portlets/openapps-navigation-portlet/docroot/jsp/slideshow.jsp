<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<script type="text/javascript" src="/openapps-navigation-portlet/js/jquery.jcarousel.js"></script>
<script>
(function($) {
    $(function() {
    	$('.jcarousel').on('jcarousel:createend', function() {
            $(this).jcarousel('scroll', 0, false);
        });
        $('.jcarousel').jcarousel({
        	wrap: 'circular'
        });
        $('.jcarousel').jcarouselAutoscroll({
            interval: <c:out value="${speed}" />
        });
        $('.jcarousel-control-prev')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '-=1'
            });
        $('.jcarousel-control-next')
            .on('jcarouselcontrol:active', function() {
                $(this).removeClass('inactive');
            })
            .on('jcarouselcontrol:inactive', function() {
                $(this).addClass('inactive');
            })
            .jcarouselControl({
                target: '+=1'
            });
        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .jcarouselPagination();
    });
})(jQuery);
</script>
<div class="jcarousel-wrapper"> 
	<div data-jcarousel="true" class="jcarousel"> 
		<ul style="left: 0px; top: 0px;">
			<c:forEach items="${images}" var="image" varStatus="status">    
				<li>
					<img width="540" height="600" src="<c:out value="${image.url}" />" alt="">
					<div class="carousel-caption-title"><c:out value="${image.title}" /></div>
					<div class="carousel-caption-descr"><c:out value="${image.description}" /></div>
				</li>
			</c:forEach>
		</ul> 
	</div> 
</div>