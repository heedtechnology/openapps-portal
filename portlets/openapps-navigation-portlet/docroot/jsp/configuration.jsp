<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@ taglib uri="http://liferay.com/tld/util" prefix="liferay-util" %>

<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntryConstants" %><%@
page import="com.liferay.portlet.documentlibrary.model.DLFolderConstants" %><%@
page import="com.liferay.portlet.documentlibrary.service.DLAppLocalServiceUtil" %><%@
page import="com.liferay.portlet.documentlibrary.service.DLAppServiceUtil" %><%@
page import="com.liferay.portlet.documentlibrary.NoSuchFolderException" %><%@
page import="com.liferay.portlet.documentlibrary.util.DLUtil" %><%@
page import="com.liferay.portal.kernel.repository.RepositoryException" %><%@
page import="com.liferay.portal.kernel.repository.model.FileEntry" %><%@
page import="com.liferay.portal.kernel.repository.model.FileVersion" %><%@
page import="com.liferay.portal.kernel.repository.model.Folder" %><%@
page import="com.liferay.portal.kernel.util.FastDateFormatFactoryUtil" %><%@
page import="com.liferay.portal.kernel.util.HtmlUtil" %><%@
page import="com.liferay.portal.kernel.util.ParamUtil" %><%@
page import="com.liferay.portal.kernel.util.PrefsParamUtil" %>

<%@ page import="java.util.List" %><%@
page import="java.util.ArrayList" %><%@
page import="java.text.Format" %>

<%@ page import="javax.portlet.MimeResponse" %><%@
page import="javax.portlet.PortletConfig" %><%@
page import="javax.portlet.PortletContext" %><%@
page import="javax.portlet.PortletException" %><%@
page import="javax.portlet.PortletMode" %><%@
page import="javax.portlet.PortletPreferences" %><%@
page import="javax.portlet.PortletRequest" %><%@
page import="javax.portlet.PortletResponse" %><%@
page import="javax.portlet.PortletURL" %><%@
page import="javax.portlet.ResourceURL" %><%@
page import="javax.portlet.UnavailableException" %><%@
page import="javax.portlet.ValidatorException" %><%@
page import="javax.portlet.WindowState" %>

<%@ page import="com.liferay.portal.kernel.language.LanguageUtil" %><%@
page import="com.liferay.portal.kernel.util.Constants" %><%@
page import="com.liferay.portal.kernel.util.GetterUtil" %><%@
page import="com.liferay.portal.kernel.util.ParamUtil" %><%@
page import="com.liferay.portal.kernel.util.StringBundler" %><%@
page import="com.liferay.portal.kernel.util.StringPool" %><%@
page import="com.liferay.portal.kernel.util.Validator" %><%@
page import="com.liferay.portlet.PortletPreferencesFactoryUtil" %>

<liferay-theme:defineObjects />

<portlet:defineObjects />

<%
PortletPreferences preferences = renderRequest.getPreferences();

String portletResource = ParamUtil.getString(request, "portletResource");

if (Validator.isNotNull(portletResource)) {
	preferences = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
}

String view = PrefsParamUtil.getString(preferences, request, "view", "");

String image1 = PrefsParamUtil.getString(preferences, request, "image1", "");
String title1 = PrefsParamUtil.getString(preferences, request, "title1", "");
String description1 = PrefsParamUtil.getString(preferences, request, "description1", "");
String url1 = PrefsParamUtil.getString(preferences, request, "url1", "");

String image2 = PrefsParamUtil.getString(preferences, request, "image2", "");
String title2 = PrefsParamUtil.getString(preferences, request, "title2", "");
String description2 = PrefsParamUtil.getString(preferences, request, "description2", "");
String url2 = PrefsParamUtil.getString(preferences, request, "url2", "");

String image3 = PrefsParamUtil.getString(preferences, request, "image3", "");
String title3 = PrefsParamUtil.getString(preferences, request, "title3", "");
String description3 = PrefsParamUtil.getString(preferences, request, "description3", "");
String url3 = PrefsParamUtil.getString(preferences, request, "url3", "");

String image4 = PrefsParamUtil.getString(preferences, request, "image4", "");
String title4 = PrefsParamUtil.getString(preferences, request, "title4", "");
String description4 = PrefsParamUtil.getString(preferences, request, "description4", "");
String url4 = PrefsParamUtil.getString(preferences, request, "url4", "");

String image5 = PrefsParamUtil.getString(preferences, request, "image5", "");
String title5 = PrefsParamUtil.getString(preferences, request, "title5", "");
String description5 = PrefsParamUtil.getString(preferences, request, "description5", "");
String url5 = PrefsParamUtil.getString(preferences, request, "url5", "");

String image6 = PrefsParamUtil.getString(preferences, request, "image6", "");
String title6 = PrefsParamUtil.getString(preferences, request, "title6", "");
String description6 = PrefsParamUtil.getString(preferences, request, "description6", "");
String url6 = PrefsParamUtil.getString(preferences, request, "url6", "");

String image7 = PrefsParamUtil.getString(preferences, request, "image7", "");
String title7 = PrefsParamUtil.getString(preferences, request, "title7", "");
String description7 = PrefsParamUtil.getString(preferences, request, "description7", "");
String url7 = PrefsParamUtil.getString(preferences, request, "url7", "");

String speed = PrefsParamUtil.getString(preferences, request, "speed", "");
%>

<liferay-portlet:actionURL portletConfiguration="true" var="configurationURL" />

<aui:form action="<%= configurationURL %>" method="post">
	<aui:input name="<%= Constants.CMD %>" type="hidden" value="<%= Constants.UPDATE %>" />
		
	<aui:select label="View Selection" name="preferences--view--">
		<aui:option selected="<%= view.equals(\"slideshow.jsp\") %>" value="slideshow.jsp">Slideshow</aui:option>
		<aui:option selected="<%= view.equals(\"banner.jsp\") %>" value="banner.jsp">Banner</aui:option>
	</aui:select>
	
	<aui:input name="preferences--image1--" value="<%= image1 %>" style="width:500px;" />
	<aui:input name="preferences--title1--" value="<%= title1 %>" style="width:500px;" />
	<aui:input name="preferences--description1--" value="<%= description1 %>" style="width:500px;" />
	<aui:input name="preferences--url1--" value="<%= url1 %>" style="width:500px;" />
	
	<aui:input name="preferences--image2--" value="<%= image2 %>" style="width:500px;" />
	<aui:input name="preferences--title2--" value="<%= title2 %>" style="width:500px;" />
	<aui:input name="preferences--description2--" value="<%= description2 %>" style="width:500px;" />
	<aui:input name="preferences--url2--" value="<%= url2 %>" style="width:500px;" />
	
	<aui:input name="preferences--image3--" value="<%= image3 %>" style="width:500px;" />
	<aui:input name="preferences--title3--" value="<%= title3 %>" style="width:500px;" />
	<aui:input name="preferences--description3--" value="<%= description3 %>" style="width:500px;" />
	<aui:input name="preferences--url3--" value="<%= url3 %>" style="width:500px;" />
	
	<aui:input name="preferences--image4--" value="<%= image4 %>" style="width:500px;" />
	<aui:input name="preferences--title4--" value="<%= title4 %>" style="width:500px;" />
	<aui:input name="preferences--description4--" value="<%= description4 %>" style="width:500px;" />
	<aui:input name="preferences--url4--" value="<%= url4 %>" style="width:500px;" />
	
	<aui:input name="preferences--image5--" value="<%= image5 %>" style="width:500px;" />
	<aui:input name="preferences--title5--" value="<%= title5 %>" style="width:500px;" />
	<aui:input name="preferences--description5--" value="<%= description5 %>" style="width:500px;" />
	<aui:input name="preferences--url5--" value="<%= url5 %>" style="width:500px;" />
	
	<aui:input name="preferences--image6--" value="<%= image6 %>" style="width:500px;" />
	<aui:input name="preferences--title6--" value="<%= title6 %>" style="width:500px;" />
	<aui:input name="preferences--description6--" value="<%= description6 %>" style="width:500px;" />
	<aui:input name="preferences--url6--" value="<%= url6 %>" style="width:500px;" />
	
	<aui:input name="preferences--image7--" value="<%= image7 %>" style="width:500px;" />
	<aui:input name="preferences--title7--" value="<%= title7 %>" style="width:500px;" />
	<aui:input name="preferences--description7--" value="<%= description7 %>" style="width:500px;" />
	<aui:input name="preferences--url7--" value="<%= url7 %>" style="width:500px;" />
	
	<aui:input name="preferences--speed--" label="Speed (milliseconds)" value="<%= speed %>" style="width:200px;" />
	
	<aui:button-row>
		<aui:button type="submit" />
	</aui:button-row>
</aui:form>