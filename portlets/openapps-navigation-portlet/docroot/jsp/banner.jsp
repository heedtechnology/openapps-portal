<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<script type="text/javascript" src="/openapps-navigation-portlet/js/jquery.jcarousel.js"></script>
<script>
(function($) {
    $(function() {
    	$('.jcarousel-banner').on('jcarousel:createend', function() {
            $(this).jcarousel('scroll', 0, false);
        });
        $('.jcarousel-banner').jcarousel({
        	wrap: 'circular',
        	vertical: true
        });
        
        $('.jcarousel-banner').jcarouselAutoscroll({
            interval: <c:out value="${speed}" />
        });
    });
})(jQuery);
</script>
<div class="jcarousel-wrapper"> 
	<div data-jcarousel="true" class="jcarousel-banner"> 
		<ul style="left: 0px; top: 0px;">
			<c:forEach items="${images}" var="image" varStatus="status">    
				<li>
					<a href="<c:out value="${image.link}" />">
						<img width="530" height="92" src="<c:out value="${image.url}" />" alt="">
					</a>
				</li>
			</c:forEach>
		</ul> 
	</div> 
</div>