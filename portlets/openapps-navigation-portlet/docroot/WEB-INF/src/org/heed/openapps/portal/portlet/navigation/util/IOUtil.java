package org.heed.openapps.portal.portlet.navigation.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;

public class IOUtil {

	/**
	 * Pipes the contents of an <code>InputStream</code> into the 
	 * specified <code>OutputStream</code>
	 * 
	 * @param in the <code>InputStream</code> to read from.
	 * @param out the <code>OutputStream</code> to write to.
	 * 
	 * @throws IOException
	 */
	public static void pipe(InputStream in, OutputStream out) throws IOException {
		byte buffer[] = new byte[8192];
		while(true){
			int len = in.read(buffer,0,8192);
			if(len < 0) break;
			out.write(buffer,0,len);
		}
	}
	
	
	/**
	 * Conceverts the specified <code>InputStream</code> into a string.
	 * 
	 * @param is the <code>InputStream</code> to read from
	 * 
	 * @return the resulting string that was read from the stream
	 * 
	 * @throws IOException
	 */
	public static String convertStreamToString(InputStream is) throws IOException {
		if (is != null) {
			Writer writer = new StringWriter();
			char[] buffer = new char[1024];
			try {
				Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
				int n;
				while ((n = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, n);
				}
			} finally {
				is.close();
			}
			return writer.toString();
		} else {        
			return "";
		}
	}
}
