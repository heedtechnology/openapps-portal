package org.heed.openapps.portal.model;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Layout;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

public class PortalPage {
	private static Log log = LogFactoryUtil.getLog(PortalPage.class);
	private long id;
	private String name;
	private String url;
	private String friendlyUrl;
	private String icon;
	private String cssClass;
	private String content;
	private String css;
	private List<PortalPage> children = new ArrayList<PortalPage>();
	
	
	public PortalPage(ThemeDisplay themeDisplay, Layout layout, String cssClass) {
		try {
			this.cssClass = cssClass;
			this.id = layout.getLayoutId();
			this.name = layout.getName(themeDisplay.getLocale());
			this.url = PortalUtil.getLayoutURL(layout, themeDisplay);
			this.friendlyUrl = layout.getFriendlyURL();
			if(layout.getIconImageId() == 0) {
				Layout parent = LayoutLocalServiceUtil.getLayout(layout.getParentPlid());
				this.icon = "/image/layout_icon?img_id="+ parent.getIconImageId();
			} else 
				this.icon = "/image/layout_icon?img_id="+ layout.getIconImageId();
		
		} catch(Exception e) {
			log.error("", e);
		}
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getIcon() {
		return icon;
	}
	public void setIcon(String icon) {
		this.icon = icon;
	}
	public String getCssClass() {
		return cssClass;
	}
	public void setCssClass(String cssClass) {
		this.cssClass = cssClass;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public List<PortalPage> getChildren() {
		return children;
	}
	public void setChildren(List<PortalPage> children) {
		this.children = children;
	}
	public String getFriendlyUrl() {
		return friendlyUrl;
	}
	public void setFriendlyUrl(String friendlyUrl) {
		this.friendlyUrl = friendlyUrl;
	}
	public String getCss() {
		return css;
	}
	public void setCss(String css) {
		this.css = css;
	}	
}
