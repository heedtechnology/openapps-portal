package org.heed.openapps.portal.model;

public class Image {
	private String url;
	private String title;
	private String description;
	private String link;
	
	public Image() {}
	public Image(String url) {
		this.url = url;
	}
	public Image(String url, String title, String description, String link) {
		this.url = url;
		this.title = title;
		this.description = description;
		this.link = link;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	
}
