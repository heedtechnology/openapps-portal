package org.heed.openapps.portal.portlet.navigation;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.heed.openapps.portal.model.Image;

import com.liferay.portal.kernel.util.PrefsParamUtil;


public class NavigationPortlet extends PortletSupport {
	//private static Log log = LogFactoryUtil.getLog(NavigationPortlet.class);
	
	
	public void doView(RenderRequest renderRequest, RenderResponse renderResponse) throws IOException, PortletException {
		//ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		//HttpServletRequest httpReq = PortalUtil.getHttpServletRequest(renderRequest);
		//HttpServletRequest httpReq2 = PortalUtil.getOriginalServletRequest(httpReq);
		
		String view = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "view", "slideshow.jsp");
		
		List<Image> images = new ArrayList<Image>();
		String image1 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "image1", null);
		String title1 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "title1", null);
		String descr1 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "description1", null);
		String url1 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "url1", null);
		
		String image2 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "image2", null);
		String title2 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "title2", null);
		String descr2 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "description2", null);
		String url2 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "url2", null);
		
		String image3 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "image3", null);
		String title3 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "title3", null);
		String descr3 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "description3", null);
		String url3 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "url3", null);
		
		String image4 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "image4", null);
		String title4 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "title4", null);
		String descr4 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "description4", null);
		String url4 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "url4", null);
		
		String image5 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "image5", null);
		String title5 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "title5", null);
		String descr5 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "description5", null);
		String url5 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "url5", null);
		
		String image6 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "image6", null);
		String title6 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "title6", null);
		String descr6 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "description6", null);
		String url6 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "url6", null);
		
		String image7 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "image7", null);
		String title7 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "title7", null);
		String descr7 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "description7", null);
		String url7 = PrefsParamUtil.getString(renderRequest.getPreferences(), renderRequest, "url7", null);
		
		int speed = PrefsParamUtil.getInteger(renderRequest.getPreferences(), renderRequest, "speed", 10000);
		
		if(image1 != null && image1.length() > 0) images.add(new Image(image1, title1, descr1, url1));
		if(image2 != null && image2.length() > 0) images.add(new Image(image2, title2, descr2, url2));
		if(image3 != null && image3.length() > 0) images.add(new Image(image3, title3, descr3, url3));
		if(image4 != null && image4.length() > 0) images.add(new Image(image4, title4, descr4, url4));
		if(image5 != null && image5.length() > 0) images.add(new Image(image5, title5, descr5, url5));
		if(image6 != null && image6.length() > 0) images.add(new Image(image6, title6, descr6, url6));
		if(image7 != null && image7.length() > 0) images.add(new Image(image7, title7, descr7, url7));
		
		renderRequest.setAttribute("speed", speed);
		renderRequest.setAttribute("images", images);
		
		include("/jsp/"+view, renderRequest, renderResponse);
	}
	
}
